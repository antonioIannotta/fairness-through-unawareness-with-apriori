# Fairness-through-unawareness-with-apriori
This repository provides a methodologically approach to mitigate bias in algorithm through a pre-processing techniques that has the goal to remove both protected attributes and proxies.
The dataset on which this analytics work has been performed are the following ones:
* [Adult Dataset](https://archive.ics.uci.edu/dataset/2/adult): about this dataset it's important to say that the output of dataset itself was binary but itself
* [Nursery Dataset](https://archive.ics.uci.edu/dataset/76/nursery): in this case we performed a transformation of the output in order to trnasforma  multi label classification problem into a matching problem. Referring to the specific domain as specified in the dataset link the label **recommend**, **priority** and **very_recom** have been replaced with 1, the others with 0. So the 1 means that the candidate is eligible, with several degrees of acceptance for the job. 0 means it isn't.
* [Obesity Dataset](https://archive.ics.uci.edu/dataset/544/estimation+of+obesity+levels+based+on+eating+habits+and+physical+condition) in this case, as in the nursery scenario, we performed a transformation of the output in order to only get the information about a patient being either affected by obesity or not. So every degrees of obesity have been labelled as 1, the other cases as 0.

For each of these datasets has been computed:
* no-fair analysis where no previous assumptions on fairness are made
* fairness-through-unawareness analysis where only the protected attributes are removed
* conscious-fairness-through-unawareness analysis where both protected attributes and proxies are removed


## Algorithm Idea
The algorithm idea impact the third analysis, where not only the protected attributes are removed but proxies too. Through this algorithm the goal is to detect the proxies using an association rule mining algorithm, **apriori**. More specifically through this algorithm it's possible to detect which variables with which values comes with other variables with a specific level of confidence.
Through a filtering for the protected attributes only, it's possible to compute the **DI** value for each attribute and detects as proxies only the attributes with a value lesser than 0.8 and greater than 1.25 
